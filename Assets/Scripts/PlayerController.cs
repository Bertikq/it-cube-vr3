﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float speed;
    private CharacterController characterController;

    [SerializeField]
    private float gravity;

    [SerializeField]
    private WeaponController weaponController;

    private void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    private void Update()
    {
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");

        Vector3 direction = new Vector3(horizontal, 0, vertical);

        direction = (transform.rotation * direction).normalized;

        if (!characterController.isGrounded)
        {
            direction.y = gravity;
        }

        characterController.Move(direction * speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ammo"))
        {
            weaponController.AddAmmo(other.
                GetComponent<AmmoBoxModel>().countAmmo);
            Destroy(other.gameObject);  
        }
    }
}

