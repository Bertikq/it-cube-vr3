﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private float speed;
    [SerializeField]
    private float distanceVisibility;
    [SerializeField]
    private float distanceAttack;
    [SerializeField]
    private float timeAttack;
    [SerializeField]
    private Transform player;
    private Animator animator;
    [SerializeField]
    private Transform[] points;

    private int curIndexPoint;

    private bool isAttack;

    private int hp;

    private void Start()
    {
        curIndexPoint = 0;
        animator = GetComponent<Animator>();
        isAttack = false;
        hp = 100;
    }

    public void Damage(int damage)
    {
        hp -= damage;
        if (hp <= 0)
        {
            Destroy(gameObject);
        }
    }

    public IEnumerator Attack()
    {
        isAttack = true;
        animator.SetTrigger("Attack");
        yield return new WaitForSeconds(timeAttack);
        isAttack = false;
    }

    private void Update()
    {
        float distance = 
            Vector3.Distance(transform.position, player.position);

        if (distance <= distanceAttack)
        {
            animator.SetBool("IsWalk", false);
            if (!isAttack)
            {
                StartCoroutine(Attack());
            }
        }
        else if (distance <= distanceVisibility)
        {
            transform.LookAt(new Vector3(
                player.position.x, 
                transform.position.y, 
                player.position.z));
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
            animator.SetBool("IsWalk", true);
        }
        else
        {
            Transform point = points[curIndexPoint];
            transform.LookAt(new Vector3(
                point.position.x,
                transform.position.y,
                point.position.z));
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
            animator.SetBool("IsWalk", true);
            if (Vector3.Distance(transform.position, point.position) < 2)
            {
                curIndexPoint = (curIndexPoint + 1) % points.Length;
            }
        }
    }
}
